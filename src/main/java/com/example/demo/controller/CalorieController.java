package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.example.demo.model.CalorieModel;

@Controller
public class CalorieController implements WebMvcConfigurer{
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/teamregistration").setViewName("teamregistration");
    }
    @GetMapping("/")
    public String index() {
       return "index.html";
       
    }
    
    @GetMapping("/teamcaptain")
    public String teamcaptain(CalorieModel caloriemodel) {
      
       return "redirect:/teamcaptainregform";
    }
    @GetMapping("/teamcaptainregform")
    public String captainregform(CalorieModel caloriemodel) {
      
       return "teamcaptainregform";
    }
    @PostMapping("/addteamname")
    public String addteamname (@Valid CalorieModel caloriemodel, BindingResult bindingResult,Model model) {
	if (bindingResult.hasErrors()) {
            return "addteamname";
        }
	model.addAttribute("teamlead", caloriemodel.getTeamlead());
       return "redirect:/teamregistration";
    }
}
