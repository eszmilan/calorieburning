package com.example.demo.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CalorieModel {
   
 
    @NotNull
    @NotEmpty
    private String teamlead;
    @NotNull
    @NotEmpty
    private String teamname;
    @NotNull
    @NotEmpty
    private String firstperson;
    @NotNull
    @NotEmpty
    private String secondperson;
    @NotNull
    @NotEmpty
    private String thirdperson;
    @NotNull
    @NotEmpty
    private String fourthperson;
    @NotNull
    @NotEmpty
    private String fifthperson;
    
    public String getTeamname() {
        return teamname;
    }
    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }
    public String getFirstperson() {
        return firstperson;
    }
    public void setFirstperson(String firstperson) {
        this.firstperson = firstperson;
    }
    public String getSecondperson() {
        return secondperson;
    }
    public void setSecondperson(String secondperson) {
        this.secondperson = secondperson;
    }
    public String getThirdperson() {
        return thirdperson;
    }
    public void setThirdperson(String thirdperson) {
        this.thirdperson = thirdperson;
    }
    public String getFourthperson() {
        return fourthperson;
    }
    public void setFourthperson(String fourthperson) {
        this.fourthperson = fourthperson;
    }
    public String getFifthperson() {
        return fifthperson;
    }
    public void setFifthperson(String fifthperson) {
        this.fifthperson = fifthperson;
    }
    public String getTeamlead() {
        return teamlead;
    }
    public void setTeamlead(String teamlead) {
	this.teamlead = teamlead;
    }
    @Override
    public String toString() {
	return "CalorieModel [teamlead=" + teamlead + ", teamname=" + teamname + ", firstperson=" + firstperson
		+ ", secondperson=" + secondperson + ", thirdperson=" + thirdperson + ", fourthperson=" + fourthperson
		+ ", fifthperson=" + fifthperson + "]";
    }
    
    
    
    }
    
    
    
    
    


