package com.example.demo.model;

import javax.validation.constraints.NotBlank;

public class TeamCaptainModel {
@NotBlank (message = "Please fill this field with valid data")
    
    private String name;

public String getName() {
    return name;
}

public void setName(String name) {
    this.name = name;
}



}
